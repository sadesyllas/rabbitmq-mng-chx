#!/bin/bash

VERSION="3.8.9-1.0.0"

buildah rmi -f registry.gitlab.com/sadesyllas/rabbitmq-mng-chx:${VERSION}

buildah bud --no-cache -t registry.gitlab.com/sadesyllas/rabbitmq-mng-chx:${VERSION} . && \
  buildah push registry.gitlab.com/sadesyllas/rabbitmq-mng-chx:${VERSION}
